#![allow(unused)]

use serde::{Deserialize, Serialize};
use std::io::{self, prelude::*, BufReader};
use std::mem::take;
use std::net::{IpAddr, Shutdown, SocketAddr, TcpStream};
use std::process::{Child, Command};
use std::str::{self, FromStr};
use std::thread;
use std::time::{self, Duration};
const BINARY_PATH: &str = env!("CARGO_BIN_EXE_cdn");

#[derive(Serialize, Debug)]
pub struct Request {
    #[serde(skip_serializing_if = "check_none")]
    student_name: String,

    #[serde(skip_serializing_if = "check_none")]
    pub request_type: String,

    #[serde(skip_serializing_if = "check_none")]
    pub key: String,

    #[serde(skip_serializing_if = "check_none")]
    pub hash: String,
}
fn check_none(str: &String) -> bool {
    if str.is_empty() {
        return true;
    }
    false
}
impl Request {
    pub fn new() -> Request {
        Request {
            student_name: ("".to_string()),
            request_type: ("".to_string()),
            key: ("".to_string()),
            hash: ("".to_string()),
        }
    }
    pub fn write(&self, stream: &mut TcpStream) -> Result<(), std::io::Error> {
        stream.write_all(serde_json::to_string(&self).unwrap().as_bytes())
    }
}
#[derive(Deserialize, Debug)]

struct Response {
    response_status: String,
    #[serde(default = "default_val")]
    requested_key: String,
    #[serde(default = "default_val")]
    requested_hash: String,
    #[serde(default = "default_val")]
    student_name: String,
}
fn default_val() -> String {
    "NULL".to_string()
}
fn take_a_nap() {
    thread::sleep(Duration::from_millis(100));
}

enum IpVersion {
    V4,
    V6,
}

struct ServerWrapper {
    proc: Option<Child>,
    addr: SocketAddr,
}

impl ServerWrapper {
    fn start(ip_version: IpVersion) -> Self {
        // let mut rng = rand::thread_rng();
        let port = 4454;
        let ip = match ip_version {
            IpVersion::V4 => IpAddr::from_str("127.0.0.1").unwrap(),
            IpVersion::V6 => IpAddr::from_str("::1").unwrap(),
        };

        eprintln!("binary path {}", BINARY_PATH);
        let proc = Command::new(BINARY_PATH).spawn().unwrap();
        thread::sleep(time::Duration::from_millis(1000));
        Self {
            proc: Some(proc),
            addr: SocketAddr::new(ip, port),
        }
    }

    fn is_alive(&mut self) -> bool {
        self.proc
            .as_mut()
            .map_or(false, |proc| proc.try_wait().unwrap().is_none())
    }

    fn stop(&mut self) -> std::io::Result<()> {
        self.proc.take().map_or(Ok(()), |mut proc| proc.kill())
    }
}

impl Drop for ServerWrapper {
    fn drop(&mut self) {
        let _ = self.stop().unwrap();
    }
}

#[derive(Debug)]
struct Client {
    name: Option<Vec<u8>>,
    conn: TcpStream,
}

impl Clone for Client {
    fn clone(&self) -> Self {
        Self {
            name: self.name.clone(),
            conn: self.conn.try_clone().unwrap(),
        }
    }
}

impl Client {
    fn name(&self) -> &[u8] {
        self.name.as_ref().unwrap()
    }
    fn start(server_addr: SocketAddr) -> std::io::Result<Self> {
        let conn = TcpStream::connect(server_addr).unwrap();
        Ok(Self { name: None, conn })
    }

    fn write(&mut self, req: Request) -> std::io::Result<()> {
        req.write(&mut self.conn)
    }

    fn shutdown(&mut self, how: Shutdown) {
        let _ = self.conn.shutdown(how);
    }

    fn read(&mut self) -> Response {
        let reader = BufReader::new(&self.conn);
        let mut json_reader = serde_json::Deserializer::from_reader(reader);
        let json = Response::deserialize(&mut json_reader).unwrap();
        return json;
    }
}

println!("store f");

#[test]
fn test_store_key() {
    let mut server = ServerWrapper::start(IpVersion::V4);
    let mut Evgenii: Client = Client::start(server.addr).unwrap();
    let mut req: Request = Request::new();
    req.request_type = "store".to_string();
    req.key = "Key!".to_string();
    req.hash = "Hash!".to_string();
    Evgenii.write(req);
    let response = Evgenii.read();
    assert_eq!(response.response_status, "success");
    assert_eq!(response.requested_hash, "NULL");
    assert_eq!(response.requested_key, "NULL");
}
#[test]

fn test_load_not_found() {
    let mut server = ServerWrapper::start(IpVersion::V4);
    let mut Evgenii: Client = Client::start(server.addr).unwrap();
    let mut req: Request = Request::new();
    req.request_type = "load".to_string();
    req.key = "Key!".to_string();
    Evgenii.write(req);
    let response = Evgenii.read();
    assert_eq!(response.response_status, "key not found");
    assert_eq!(response.requested_hash, "NULL");
    assert_eq!(response.requested_key, "NULL");
}
#[test]

fn test_load_key() {
    let mut server = ServerWrapper::start(IpVersion::V4);
    let mut Evgenii: Client = Client::start(server.addr).unwrap();
    let mut req: Request = Request::new();

    req.request_type = "store".to_string();
    req.key = "Key!".to_string();
    req.hash = "Hash!".to_string();
    Evgenii.write(req);
    let _ = Evgenii.read();
    let mut req: Request = Request::new();
    req.request_type = "load".to_string();
    req.key = "Key!".to_string();
    Evgenii.write(req);
    let response = Evgenii.read();
    assert_eq!(response.response_status, "success");
    assert_eq!(response.requested_hash, "Hash!");
    assert_eq!(response.requested_key, "Key!");
}
#[test]

fn one_loads_another_reads() {
    let mut server = ServerWrapper::start(IpVersion::V4);
    let mut Evgenii: Client = Client::start(server.addr).unwrap();
    let mut req: Request = Request::new();

    req.request_type = "store".to_string();
    req.key = "Key!".to_string();
    req.hash = "Hash!".to_string();
    Evgenii.write(req);
    let _ = Evgenii.read();

    let mut Dmitry: Client = Client::start(server.addr).unwrap();

    let mut req: Request = Request::new();
    req.request_type = "load".to_string();
    req.key = "Key!".to_string();
    Dmitry.write(req);
    let response = Dmitry.read();
    assert_eq!(response.response_status, "success");
    assert_eq!(response.requested_hash, "Hash!");
    assert_eq!(response.requested_key, "Key!");
}
#[test]

fn massive_test() {
    let mut server = ServerWrapper::start(IpVersion::V4);
    let mut Evgenii: Client = Client::start(server.addr).unwrap();
    let mut Dmitry: Client = Client::start(server.addr).unwrap();
    for i in 1..1000 {
        let mut req: Request = Request::new();
        req.request_type = "store".to_string();
        req.key = format!("Key!{i}").to_string();
        req.hash = format!("Hash!{i}").to_string();
        Evgenii.write(req);
        let _ = Evgenii.read();

        let mut req: Request = Request::new();
        req.request_type = "load".to_string();
        req.key = format!("Key!{i}").to_string();
        Dmitry.write(req);
        let response = Dmitry.read();
        assert_eq!(response.response_status, "success");
        assert_eq!(response.requested_hash, format!("Hash!{i}"));
        assert_eq!(response.requested_key, format!("Key!{i}"));
    }
}
