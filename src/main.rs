mod errors;
mod logger;
mod server;
use clap::Parser;
use std::net;
use std::str::FromStr;
#[derive(Debug, Parser)]
struct Opts {
    #[clap(short, long, default_value = "127.0.0.1")]
    ip: net::IpAddr,

    #[clap(short, long, default_value = "4454")]
    port: u16,
}

fn main() {
    let opts = Opts::parse();
    let run_err = server::run(opts.ip, opts.port);
    if run_err.is_err() {

            let err_kind = run_err.err().unwrap();
            println!("Error : {} \n\r Occured when tried to run server", err_kind);
    }
}
