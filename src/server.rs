use crate::errors::ErrorType;
use crate::logger::{self, Logger};
use serde::Deserialize;
use std::{
    collections::HashMap,
    io::{BufReader, Write},
    net::{self, IpAddr, SocketAddr, TcpStream},
    sync::{
        mpsc::{channel, Receiver, Sender},
        Arc, Mutex,
    },
};
type ClientDataStorage = Arc<Mutex<HashMap<String, String>>>;
//enum for comfort request deserialization

#[serde(rename_all = "lowercase")]
#[serde(tag = "request_type")]
#[derive(Deserialize, Debug, Clone)]
pub enum Request {
    Store { key: String, hash: String },
    Load { key: String },
}
#[derive(serde::Serialize)]
///Struct for comfort serialization of responses
struct Response {
    response_status: String,
    #[serde(skip_serializing_if = "String::is_empty")]
    requested_key: String,
    #[serde(skip_serializing_if = "String::is_empty")]
    requested_hash: String,
    #[serde(skip_serializing_if = "String::is_empty")]
    student_name: String,
}

impl Response {
    pub fn new() -> Response {
        Response {
            response_status: ("".to_string()),
            requested_key: ("".to_string()),
            requested_hash: ("".to_string()),
            student_name: ("".to_string()),
        }
    }

    pub fn write(&self, stream: &mut TcpStream) -> Result<(), std::io::Error> {
        stream.write_all(serde_json::to_string(&self).unwrap().as_bytes())
    }
}
#[derive(Debug, Clone)]
pub struct LoggingInfo {
    pub server_ip: String,
    pub req_type: String,
    pub mapsize: usize,
}
impl LoggingInfo {
    pub fn new() -> LoggingInfo {
        LoggingInfo {
            server_ip: "".to_string(),
            req_type: "".to_string(),
            mapsize: 0,
        }
    }
}

///Function that loads elemement by key and returns it to client

fn load(hashmap: ClientDataStorage, key: String) -> Result<Response, ErrorType> {
    let mut response: Response = Response::new();
    let map_guard = hashmap.lock().unwrap();
    if map_guard.contains_key(&key) {
        response.response_status = "success".to_string();
        response.requested_key = key.clone();
        response.requested_hash = map_guard.get(&key).unwrap().to_owned();
    } else {
        response.response_status = "key not found".to_string();
    }
    Ok(response)
}

///Function that loads elemement by key and returns response to client

fn store(hashmap: ClientDataStorage, key: String, hash: String) -> Result<Response, ErrorType> {

    let mut response: Response = Response::new();


    let mut map_guard = hashmap.lock().unwrap();


    map_guard.insert(key.clone(), hash.clone());

    response.response_status = "success".to_string();

    Ok(response)
}

///Function that reads client request

fn read_client_request(stream: TcpStream) -> Result<Request, ErrorType> {
    let reader = BufReader::new(&stream);
    let mut json_reader = serde_json::Deserializer::from_reader(reader);

    let json = Request::deserialize(&mut json_reader);

    if json.is_err() {

        let err_kind = std::io::ErrorKind::InvalidData;
        let new_err = std::io::Error::new(err_kind, "Not a json file");
        return Err(ErrorType::BadConnection);
    }
    return Ok(json.unwrap());
}

///Function that handles new connection
///
/// This function handles new connection in spawned ( by run() ) thread
/// In this thread we will continueosly read jsons from our stream and write our self-made jsons to it
/// Function returns error if something went wrong on reading on writing stage
/// For example - we got non json file

pub fn handle_stream(
    mut stream: TcpStream,
    hashmap: ClientDataStorage,
    sender: Sender<LoggingInfo>,
) -> Result<(), ErrorType> {
    loop {
        let json = read_client_request(stream.try_clone().unwrap());
        let mut response: Response = Response::new();
        if json.is_err() {

            return Err(json.err().unwrap());
        }

        let json = json?;
        let mut logger_info: LoggingInfo = LoggingInfo::new();
        logger_info.server_ip = stream.peer_addr().unwrap().to_string();
        let guard = hashmap.lock().unwrap();
        logger_info.mapsize = guard.len();
        drop(guard);

        match json {
            Request::Store { key, hash } => {
                logger_info.req_type = "store".to_string();
                response = store(hashmap.clone(), key, hash)?;

            }
            Request::Load { key } => {

                logger_info.req_type = "load".to_string();
                response = load(hashmap.clone(), key)?;
            }
        }

        sender.send(logger_info).unwrap();

        response.write(&mut stream)?;
    }
}
///This function runs the server
///
/// If new connection arrives run will call handle_stream function in freshly spawned thread
/// If some errors occured in handle_stream, run will kill thread and give us a message
pub fn run(ip: net::IpAddr, port: u16) -> Result<(), ErrorType> {

    let server_address = net::SocketAddr::new(ip, port);
    let listener: net::TcpListener = net::TcpListener::bind(server_address)?;
    let core_hashmap: HashMap<String, String> = HashMap::new();
    let core_hashmap = Arc::new(Mutex::new(core_hashmap));

    let (sndr, rcvr): (Sender<LoggingInfo>, Receiver<LoggingInfo>) = channel();
    std::thread::spawn(move || {Logger::log(rcvr);});
    for stream in listener.incoming() {
        let stream = stream?;
        let hashmap_mutex = core_hashmap.clone();
        let cloned_sender = sndr.clone();
        std::thread::spawn(move || {
            let handled_error = handle_stream(stream, hashmap_mutex, cloned_sender);
            if handled_error.is_err() {
                println!("Error occured in stream handling! Thred is dead now");
            }
        });
    }

    return Ok(());
}
