use chrono;
use std::{
    collections::HashMap,
    fmt::Debug,
    io::Write,
    net::TcpStream,
    sync::{mpsc::Receiver, Arc, Mutex},
};

use crate::server::{self, LoggingInfo, Request};
type HashmapMutex = Arc<Mutex<HashMap<String, String>>>;
///This struct helps us logging server events
pub struct Logger {}
impl Logger {
    ///Function that actually do logging
    ///
    /// With each log it prints time and current mapsize

    pub fn log(recver: Receiver<LoggingInfo>) {
        loop {
            println!("entered logger loop");
            let json = recver.recv().unwrap();
            println!("{}", json.server_ip);
            let dt = chrono::Local::now();
            print!("{} ", dt);
            if json.req_type == "load" {
                print!("Received request to get value by key");
            } else if json.req_type == "store" {
                print!("Received request to write new value.");
            }
            print!(" Current mapsize = {}\r\n", json.mapsize);
            std::io::stdout().flush().unwrap();
        }
    }
}
